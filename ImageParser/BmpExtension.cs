﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace BitmapExtensions
{
    /// <summary>
    /// Класс для работы с изображениями
    /// </summary>
    public static class BmpExtension
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);
        /// <summary>
        /// Сохранить ихображение в формате png
        /// </summary>
        /// <param name="img">Изображение</param>
        /// <param name="fileName">Имя файла</param>
        public static void SavePng(this BitmapSource img, String fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
            {
                BitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(img));
                encoder.Save(fileStream);
            }
        }

        /// <summary>
        /// Сохранить ихображение в формате jpeg
        /// </summary>
        /// <param name="img">Изображение</param>
        /// <param name="fileName">Имя файла</param>
        public static void SaveJpeg(this BitmapSource img, String fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
            {
                BitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(img));
                encoder.Save(fileStream);
            }
        }
        /// <summary>
        /// Сохранить ихображение в формате bmp
        /// </summary>
        /// <param name="img">Изображение</param>
        /// <param name="fileName">Имя файла</param>
        public static void SaveBmp(this BitmapSource img, String fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
            {
                BitmapEncoder encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(img));
                encoder.Save(fileStream);
            }
        }
        /// <summary>
        /// Перевести изображение в формат Bitmap
        /// </summary>
        /// <param name="bitmapsource">изображение BitmapSource</param>
        /// <returns>изображение Bitmap</returns>
        public static System.Drawing.Bitmap ToBitmap(this BitmapSource bitmapsource)
        {
            System.Drawing.Bitmap bitmap;
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
            }
            return bitmap;
        }
        private static System.Windows.Media.PixelFormat ConvertPixelFormat(System.Drawing.Imaging.PixelFormat sourceFormat)
        {
            switch (sourceFormat)
            {
                case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                    return System.Windows.Media.PixelFormats.Bgr24;

                case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
                    return System.Windows.Media.PixelFormats.Bgra32;

                case System.Drawing.Imaging.PixelFormat.Format32bppRgb:
                    return System.Windows.Media.PixelFormats.Bgr32;
            }
            return new System.Windows.Media.PixelFormat();
        }

        /// <summary>
        /// Перевести изображение в формат BitmapSource
        /// </summary>
        /// <param name="source">изображение Bitmap</param>
        /// <returns>изображение BitmapSource</returns>
        public static BitmapSource ToBitmapSource(this System.Drawing.Bitmap source)
        {
            System.Drawing.Imaging.BitmapData data = source.LockBits(new System.Drawing.Rectangle(0, 0, source.Width, source.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, source.PixelFormat);
            WriteableBitmap res = new WriteableBitmap(source.Width, source.Height, 96, 96, ConvertPixelFormat(source.PixelFormat), null);
            res.WritePixels(new Int32Rect(0, 0, source.Width, source.Height), data.Scan0, data.Width * data.Height * res.Format.BitsPerPixel / 8, data.Stride);
            source.UnlockBits(data);
            return res;
        }
        /// <summary>
        /// Получить пиксели картинки в виде массива байтов
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <returns>Массив пикселей</returns>
        public static byte[] GetAllPixels(this BitmapSource image)
        {
            int stride = image.PixelWidth * (image.Format.BitsPerPixel / 8);
            byte[] pixels = new byte[image.PixelHeight * stride];
            image.CopyPixels(pixels, stride, 0);
            return pixels;
        }

        /// <summary>
        /// Получить пиксели картинки в виде массива байтов
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <returns>Массив пикселей</returns>
        public static byte[] GetAllPixels(this System.Drawing.Bitmap image)
        {
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, image.Width, image.Height);
            System.Drawing.Imaging.BitmapData bitmapData = image.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, image.PixelFormat);
            byte[] pixels = new byte[image.Height * image.Width * image.GetBytesPerPixel()];
            Marshal.Copy(bitmapData.Scan0, pixels, 0, pixels.Length);
            image.UnlockBits(bitmapData);
            return pixels;
        }
        /// <summary>
        /// Получить размер пикселя изображения
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <returns>Размер пикселя в байтах</returns>
        public static int GetBytesPerPixel(this BitmapSource image)
        {
            return image.Format.BitsPerPixel / 8;
        }

        /// <summary>
        /// Получить размер пикселя изображения
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <returns>Размер пикселя в байтах</returns>
        public static int GetBytesPerPixel(this System.Drawing.Bitmap image)
        {
            return System.Drawing.Bitmap.GetPixelFormatSize(image.PixelFormat) / 8;
        }
        /// <summary>
        /// Записать значения пикселей из массива в изображение
        /// </summary>
        /// <param name="wbmp">Изображение</param>
        /// <param name="pixels">Массив пикселей для записи</param>
        public static void WriteAllPixels(this WriteableBitmap wbmp, byte[] pixels)
        {
            wbmp.WritePixels(new Int32Rect(0, 0, wbmp.PixelWidth, wbmp.PixelHeight), pixels, wbmp.BackBufferStride, 0);
        }

        /// <summary>
        /// Записать значения пикселей из массива в изображение
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <param name="pixels">Массив пикселей для записи</param>
        public static void WriteAllPixels(this System.Drawing.Bitmap image, byte[] pixels)
        {
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, image.Width, image.Height);
            System.Drawing.Imaging.BitmapData bitmapData = image.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, image.PixelFormat);
            Marshal.Copy(pixels, 0, bitmapData.Scan0, pixels.Length);
            image.UnlockBits(bitmapData);
        }
    }
}
