﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageParser
{
    public static class ImageConvert
    {
        public static byte[] GetHBytes()
        {
            return null;
        }


        public static void WriteYuvBytesToH(byte[] yuv, int w, int h, string filePath)
        {
            StringBuilder sb = new StringBuilder();
            string imgName = Path.GetFileNameWithoutExtension(filePath);
            sb.Append($"#ifndef {imgName.ToUpper()}_H\r\n");
            sb.Append($"#define {imgName.ToUpper()}_H\r\n");
            sb.AppendLine();
            sb.Append($"#define {imgName.ToUpper()}_WIDTH ({w})\r\n");
            sb.Append($"#define {imgName.ToUpper()}_HEIGHT ({h})\r\n");
            sb.AppendLine();
            sb.Append($"unsigned char {imgName}_y[]={{");
            int count = 0;
            int sz = w * h;
            for (int i= 0; i<sz;i++){
                byte yByte = yuv[i];
                if (count % 15 == 0)
                {
                    count = 0;
                    sb.AppendLine();
                    sb.Append("    ");
                }
                sb.AppendFormat("0x{0:X2}, ",yByte);
                count++;
            }
            sb.Length -= 2;
            sb.AppendLine("};");
            sb.AppendLine();
            sb.Append($"unsigned char {imgName}_uv[]={{");
            count = 0;
            for (int i=sz; i<yuv.Length;i++)
            {
                byte uvByte = yuv[i];
                if (count % 15 == 0)
                {
                    count = 0;
                    sb.AppendLine();
                    sb.Append("    ");
                }
                sb.AppendFormat("0x{0:X2}, ", uvByte);
                count++;
            }
            sb.Length -= 2;
            sb.AppendLine("};");
            sb.AppendLine();
            sb.AppendLine("#endif");
            File.WriteAllText(filePath, sb.ToString());
        }

        public static byte[] GetYuvFromH(string filePath, out int w, out int h)
        {
            string imgName = Path.GetFileNameWithoutExtension(filePath);
            string content = File.ReadAllText(filePath);
            w = 0;
            h = 0;
            return null;
        }
    }
}
