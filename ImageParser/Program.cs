﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using YuvFormatter;
using BitmapExtensions;

namespace ImageParser
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 4)
            {
                Console.WriteLine("Укажите аргументы: папка формат папка формат [ширина] [высота]");
                Console.WriteLine("Поддерживаемые форматы yuv rgb png h, при форматах yuv rgb необходимо указать ширину и высоту");
            }
            else if (!Directory.Exists(args[0]))
            {
                Console.WriteLine($"Директория {args[0]} не обнаружена");
            }
            else
            {
                string[] rgbFiles;
                int w = 0;
                int h = 0;
                switch (args[1])
                {
                    case "rgb":
                        w = int.Parse(args[4]);
                        h = int.Parse(args[5]);
                        rgbFiles = Directory.GetFiles(args[0], "*.y"); break;
                    case "png":
                        rgbFiles = Directory.GetFiles(args[0], "*.png"); break;
                    case "h":
                        rgbFiles = Directory.GetFiles(args[0], "*.h"); break;
                    default:
                        w = int.Parse(args[4]);
                        h = int.Parse(args[5]);
                        rgbFiles = Directory.GetFiles(args[0], "*.yuv"); break; /*yuv*/
                }
                Directory.CreateDirectory(args[2]);
                foreach (string rgbFile in rgbFiles)
                {
                    byte[] yuvBytes;
                    switch (args[1])
                    {
                        case "rgb":
                            {
                                byte[] rgbBytes = File.ReadAllBytes(rgbFile);
                                yuvBytes = YuvConverter.FromBGR(rgbBytes, w, h);
                                string outFileNoExt2 = Path.Combine(args[2], Path.GetFileNameWithoutExtension(rgbFile));
                                if (w * h * 3 < rgbBytes.Length)
                                {
                                    int size = w * h * 3;
                                    int imageCount = rgbBytes.Length / size;
                                    for(int i=0; i<imageCount; i++)
                                    {
                                        byte[] subImage = new byte[size];
                                        Array.Copy(rgbBytes, i * size, subImage, 0, size);
                                        byte[] subYuvBytes= YuvConverter.FromBGR(subImage, w, h);
                                        ImageConvert.WriteYuvBytesToH(subYuvBytes, w, h, $"{outFileNoExt2}_{i}.h");
                                    }
                                }
                            }
                            break;
                        case "png":
                            BitmapImage img = new BitmapImage(new Uri(rgbFile,UriKind.Relative));
                            w = img.PixelWidth;
                            h = img.PixelHeight;
                            yuvBytes = YuvConverter.FromBitmap(img);
                            break;
                        case "h":
                            throw new NotImplementedException("Пока не сделал");
                        default:
                            yuvBytes = File.ReadAllBytes(rgbFile);
                            break;
                    }
                    string outFileNoExt = Path.Combine(args[2], Path.GetFileNameWithoutExtension(rgbFile));
                    switch (args[3])
                    {
                        case "rgb":
                            {
                                byte[] rgbBytes = YuvConverter.BgrFromYuv(yuvBytes, w, h);
                                File.WriteAllBytes($"{outFileNoExt}.y", rgbBytes);
                            }
                            break;
                        case "png":
                            BitmapImage img = YuvConverter.SourceFromYuv(yuvBytes, w, h);
                            img.SavePng($"{outFileNoExt}.png");
                            break;
                        case "h":
                            ImageConvert.WriteYuvBytesToH(yuvBytes, w, h, $"{outFileNoExt}.h");
                            break;
                        default:
                            File.WriteAllBytes($"{outFileNoExt}.yuv", yuvBytes);
                            break;
                    }
                }
            }
        }
    }
}

