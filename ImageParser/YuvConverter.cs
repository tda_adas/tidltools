﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace YuvFormatter
{
    /// <summary>
    /// Статический класс для перевода изображений из и в YUV формат
    /// </summary>
    public static class YuvConverter
    {
        /// <summary>
        /// Обрезать число до размера беззнакогового байтового числа (byte)
        /// </summary>
        /// <param name="val">Число для обрезания</param>
        /// <returns>Число в пределах от 0 до 255</returns>
        private static byte Clamp(double val)
        {
            if (val > 255) return 255;
            else if (val < 0) return 0;
            else return (byte)val;
        }
        /// <summary>
        /// Обрезать число до размера знакогового байтового числа (sbyte)
        /// </summary>
        /// <param name="val">Число для обрезания</param>
        /// <returns>Число в пределах от -128 до 127</returns>
        private static sbyte ClampSbyte(int val)
        {
            if (val > 127) return 127;
            else if (val < -128) return -128;
            else return (sbyte)val;
        }
        /// <summary>
        /// Получить составляющую Y (luma) из  RGB
        /// </summary>
        /// <param name="r">Красная составляющая</param>
        /// <param name="g">Зелёная составляющая</param>
        /// <param name="b">Синяя составляющая</param>
        /// <returns>Y составляющая из YUV</returns>
        private static byte GetY(byte r, byte g, byte b)
        {
            return Clamp(((66 * r + 129 * g + 25 * b + 128) >> 8) + 16);
        }
        /// <summary>
        /// Получить составляющую U из  RGB
        /// </summary>
        /// <param name="r">Красная составляющая</param>
        /// <param name="g">Зелёная составляющая</param>
        /// <param name="b">Синяя составляющая</param>
        /// <returns>U составляющая из YUV</returns>
        private static byte GetU(byte r, byte g, byte b)
        {
            return Clamp(((-38 * r - 74 * g + 112 * b + 128) >> 8) + 128);
        }
        /// <summary>
        /// Получить составляющую V из  RGB
        /// </summary>
        /// <param name="r">Красная составляющая</param>
        /// <param name="g">Зелёная составляющая</param>
        /// <param name="b">Синяя составляющая</param>
        /// <returns>V составляющая из YUV</returns>
        private static byte GetV(byte r, byte g, byte b)
        {
            return Clamp(((112 * r - 94 * g - 18 * b + 128) >> 8) + 128);
        }
        /// <summary>
        /// Получить красную составляющую из преобразованного YUV
        /// </summary>
        /// <param name="c">Преобразованный Y (Y - 16)</param>
        /// <param name="d">Преобразованный U (U - 128)</param>
        /// <param name="e">Преобразованный V (V - 128)</param>
        /// <returns>Красная составляющая из RGB</returns>
        private static byte GetR(int c, int d, int e)
        {
            return Clamp((298 * c + 409 * e + 128) >> 8);
        }
        /// <summary>
        /// Получить зелёную составляющую из преобразованного YUV
        /// </summary>
        /// <param name="c">Преобразованный Y (Y - 16)</param>
        /// <param name="d">Преобразованный U (U - 128)</param>
        /// <param name="e">Преобразованный V (V - 128)</param>
        /// <returns>Зелёная составляющая из RGB</returns>
        private static byte GetG(int c, int d, int e)
        {
            return Clamp((298 * c - 100 * d - 208 * e + 128) >> 8);
        }
        /// <summary>
        /// Получить синюю составляющую из преобразованного YUV
        /// </summary>
        /// <param name="c">Преобразованный Y (Y - 16)</param>
        /// <param name="d">Преобразованный U (U - 128)</param>
        /// <param name="e">Преобразованный V (V - 128)</param>
        /// <returns>Синяя составляющая из RGB</returns>
        private static byte GetB(int c, int d, int e)
        {
            return Clamp((298 * c + 516 * d + 128) >> 8);
        }

        /// <summary>
        /// Преобразовать массив bgr Planar в YUV
        /// </summary>
        /// <param name="bgrBytes">Массив пикселей планарный</param>
        /// <param name="width">Ширина картинки</param>
        /// <param name="height">Высота картинки</param>
        /// <returns></returns>
        public static byte[] FromBGR(byte[] bgrBytes, int width, int height)
        {
            byte[] res = new byte[width * height * 3 / 2];
            int size = width * height;
            unsafe
            {
                bool evenRow = true;
                bool evenColumn = false;
                int lumaPos = 0;
                int chromaPos = width * height;
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        int pos = x + y * width;
                        byte blue = bgrBytes[pos + size * 2];
                        byte green = bgrBytes[pos + size];
                        byte red = bgrBytes[pos ];
                        res[lumaPos] = GetY(red, green, blue);
                        lumaPos++;
                        if (evenColumn && evenRow)
                        {
                            unchecked
                            {
                                res[chromaPos] = GetU(red, green, blue);
                                chromaPos++;
                                res[chromaPos] = GetV(red, green, blue);
                                chromaPos++;
                            }
                        }
                        evenColumn = !evenColumn;
                    }
                    evenRow = !evenRow;
                }
            }
            return res;
        }

        /// <summary>
        /// Преобразовать System.Drawing.Bitmap в yuv
        /// </summary>
        /// <param name="bmp">Входное изображение</param>
        /// <returns>Изображение YUV</returns>
        public static byte[] FromBitmap(System.Drawing.Bitmap bmp)
        {
            byte[] res = new byte[bmp.Width * bmp.Height * 3 / 2];
            unsafe
            {
                System.Drawing.Imaging.BitmapData bitmapData = bmp.LockBits(new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, bmp.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bmp.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData.Scan0;
                bool evenRow = true;
                bool evenColumn = false;
                int lumaPos = 0;
                int chromaPos = bmp.Width * bmp.Height;
                for (int y = 0; y < heightInPixels; y++)
                {
                    byte* currentLine = ptrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        byte blue = currentLine[x];
                        byte green = currentLine[x + 1];
                        byte red = currentLine[x + 2];
                        res[lumaPos] = GetY(red, green, blue);
                        lumaPos++;
                        if (evenColumn && evenRow)
                        {
                            unchecked
                            {
                                res[chromaPos] = GetU(red, green, blue);
                                chromaPos++;
                                res[chromaPos] = GetV(red, green, blue);
                                chromaPos++;
                            }
                        }
                        evenColumn = !evenColumn;
                    }
                    evenRow = !evenRow;
                }
                bmp.UnlockBits(bitmapData);
            }
            return res;
        }
        /// <summary>
        /// Преобразовать в Windows.Media.BitmapImage
        /// </summary>
        /// <param name="bmp">Входное изображение</param>
        /// <returns>Изображение YUV</returns>
        public static byte[] FromBitmap(BitmapImage bmp)
        {
            byte[] res = new byte[bmp.PixelWidth * bmp.PixelHeight * 3 / 2];
            int stride = bmp.PixelWidth * 4;
            int size = bmp.PixelHeight * stride;
            byte[] pixels = new byte[size];
            bmp.CopyPixels(pixels, stride, 0);
            bool evenRow = true;
            bool evenColumn = false;
            int lumaPos = 0;
            int chromaPos = bmp.PixelWidth * bmp.PixelHeight;
            for (int y = 0; y < bmp.PixelHeight; y++)
            {
                for (int x = 0; x < stride; x += 4)
                {
                    byte blue = pixels[y * stride + x + 2];
                    byte green = pixels[y * stride + x + 1];
                    byte red = pixels[y * stride + x];
                    res[lumaPos] = GetY(red, green, blue);
                    lumaPos++;
                    if (evenColumn && evenRow)
                    {
                        unchecked
                        {
                            res[chromaPos] = GetV(red, green, blue);
                            chromaPos++;
                            res[chromaPos] = GetU(red, green, blue);
                            chromaPos++;
                        }
                    }
                    evenColumn = !evenColumn;
                }
                evenRow = !evenRow;
            }
            return res;
        }
        /// <summary>
        /// Получить System.Drawing.Bitmap из изображения YUV
        /// </summary>
        /// <param name="yuv">Входное изображение</param>
        /// <param name="width">Ширина изображения в пикселях</param>
        /// <param name="height">Высота изображения в пикселях</param>
        /// <returns>Изображение System.Drawing.Bitmap</returns>
        public static System.Drawing.Bitmap BitmapFromYuv(byte[] yuv, int width, int height)
        {
            System.Drawing.Bitmap res = new System.Drawing.Bitmap(width, height);
            unsafe
            {
                System.Drawing.Imaging.BitmapData bitmapData = res.LockBits(new System.Drawing.Rectangle(0, 0, res.Width, res.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, res.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(res.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData.Scan0;
                bool evenColumn = true;
                bool evenRow = true;
                int pos = 0;
                int chromaPos = res.Width * res.Height;
                byte u = 0;
                byte v = 0;
                for (int lumaPos = 0; lumaPos < width * height; lumaPos++)
                {
                    byte y = yuv[lumaPos];
                    if (evenColumn)
                    {
                        u = yuv[chromaPos];
                        chromaPos++;
                        v = yuv[chromaPos];
                        chromaPos++;
                    }
                    evenColumn = !evenColumn;
                    pos++;
                    if (pos == width)
                    {
                        pos = 0;
                        if (evenRow)
                            chromaPos -= width;
                        evenRow = !evenRow;
                    }
                    int c = y - 16;
                    int d = u - 128;
                    int e = v - 128;
                    ptrFirstPixel[0] = GetB(c, d, e);
                    ptrFirstPixel[1] = GetG(c, d, e);
                    ptrFirstPixel[2] = GetR(c, d, e);
                    ptrFirstPixel[3] = 255;
                    ptrFirstPixel += 4;
                }
                res.UnlockBits(bitmapData);
            }
            return res;
        }
        /// <summary>
        /// Получить Windows.Media.BitmapImage из изображения YUV
        /// </summary>
        /// <param name="yuv">Входное изображение</param>
        /// <param name="width">Ширина изображения в пикселях</param>
        /// <param name="height">Высота изображения в пикселях</param>
        /// <returns>Изображение Windows.Media.BitmapImage</returns>
        public static BitmapImage SourceFromYuv(byte[] yuv, int width, int height)
        {
            WriteableBitmap wbmp = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null);
            byte[] pixels = new byte[width * height * 4];
            bool evenColumn = true;
            bool evenRow = true;
            int pos = 0;
            int chromaPos = width * height;
            byte u = 0;
            byte v = 0;
            for (int lumaPos = 0; lumaPos < width * height; lumaPos++)
            {
                byte y = yuv[lumaPos];
                if (evenColumn)
                {
                    u = yuv[chromaPos];
                    chromaPos++;
                    v = yuv[chromaPos];
                    chromaPos++;
                }
                evenColumn = !evenColumn;
                pos++;
                if (pos == width)
                {
                    pos = 0;
                    if (evenRow)
                        chromaPos -= width;
                    evenRow = !evenRow;
                }
                int c = y - 16;
                int d = u - 128;
                int e = v - 128;
                int pixPos = lumaPos * 4;
                pixels[pixPos] = GetB(c, d, e);
                pixels[pixPos + 1] = GetG(c, d, e);
                pixels[pixPos + 2] = GetR(c, d, e);
                pixels[pixPos + 3] = 255;
            }
            wbmp.WritePixels(new System.Windows.Int32Rect(0, 0, width, height), pixels, width * 4, 0);
            BitmapImage bmImage = new BitmapImage();
            using (MemoryStream stream = new MemoryStream())
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(wbmp));
                encoder.Save(stream);
                bmImage.BeginInit();
                bmImage.CacheOption = BitmapCacheOption.OnLoad;
                bmImage.StreamSource = stream;
                bmImage.EndInit();
                bmImage.Freeze();
            }
            return bmImage;
        }

        /// <summary>
        /// Получить RGB planar из изображения YUV
        /// </summary>
        /// <param name="yuv">Входное изображение</param>
        /// <param name="width">Ширина изображения в пикселях</param>
        /// <param name="height">Высота изображения в пикселях</param>
        /// <returns>RGB planar</returns>
        public static byte[] BgrFromYuv(byte[] yuv, int width, int height)
        {
            byte[] pixels = new byte[width * height * 3];
            bool evenColumn = true;
            bool evenRow = true;
            int pos = 0;
            int size = width * height;
            int chromaPos = width * height;
            byte u = 0;
            byte v = 0;
            for (int lumaPos = 0; lumaPos < width * height; lumaPos++)
            {
                byte y = yuv[lumaPos];
                if (evenColumn)
                {
                    u = yuv[chromaPos];
                    chromaPos++;
                    v = yuv[chromaPos];
                    chromaPos++;
                }
                evenColumn = !evenColumn;
                pos++;
                if (pos == width)
                {
                    pos = 0;
                    if (evenRow)
                        chromaPos -= width;
                    evenRow = !evenRow;
                }
                int c = y - 16;
                int d = u - 128;
                int e = v - 128;
                int pixPos = lumaPos;
                pixels[pixPos + size * 2] = GetR(c, d, e);
                pixels[pixPos + size] = GetG(c, d, e);
                pixels[pixPos] = GetB(c, d, e);
            }
            return pixels;
        }
    }
}
